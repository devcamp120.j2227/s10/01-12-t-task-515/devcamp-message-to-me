import { Component } from "react";

class InputBody extends Component {

    onInputChangeHandler(event) {
        console.log(event.target.value);
    }

    onButtonClickHandler() {
        console.log("Đã bấm nút gửi thông điệp");
    }

    render() {
        return (
            <>
                <div className='row mt-3'>
                    <label>Mesage cho bạn 12 tháng tới</label>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <input className='form-control' onChange={this.onInputChangeHandler} placeholder='Nhập vào message'/>
                    </div>        
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <button className='btn btn-primary' onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>        
                </div> 
            </>
        )
    }
}

export default InputBody