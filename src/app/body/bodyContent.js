import { Component } from "react";
import InputBody from "./input-body/inputBody";
import OutputBody from "./output-body/outputBody";


class BodyContent extends Component {
    render() {
        return (
            <>
                <InputBody/>
                <OutputBody/>
            </>
        )
    }
}

export default BodyContent