import { Component } from "react";
import like from '../../../assets/images/like-yellow-icon.png'

class OutputBody extends Component {
    render() {
        return (
            <>
                <div className='row mt-3'>
                    <p>Nội dung thông điệp</p>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <img src={like} alt='like' style={{width:"100px"}}/>
                    </div>        
                </div>
            </>
        )
    }
}

export default OutputBody