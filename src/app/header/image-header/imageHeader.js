import { Component } from "react";
import programing from '../../../assets/images/programing.png'

class ImageHeader extends Component {
    render() {
        return(
            <div className='row mt-3'>
                <div className='col-12'>
                <img src={programing} alt="programing" style={{width:"500px"}}/>
                </div>        
            </div>
        )
    }
}

export default ImageHeader