import 'bootstrap/dist/css/bootstrap.min.css'
import BodyContent from './app/body/bodyContent';
import HeaderContent from './app/header/headerContent';

function App() {
  return (
    <div className='container text-center'>
        <HeaderContent/>
        <BodyContent/>       
    </div>
  );
}

export default App;
